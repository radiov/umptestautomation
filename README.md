# UMP Automation Repository - RobotFramework + Jenkins 

## Introduction
-------------------

UMP Automation Team Test code

Folders
-------------------
- Robot =>  Robot Framework Code `Robot Framework <http://robotframework.org>`
- Robot/Resources/stages/conf_test_env.py => All configuration variables per test environment
- Robot/Libraries => All Libraries of backend components or util used on Keywords/Tests
- Robot/Keywords => All keywords used in Tests
- Robot/Tests =>  All Robot Framework Test Cases (Tests Suites)


## Where to add a new test environment details
-------------------
     robot/resources/stages/conf_test_env.py =>  Configuration File with all backend server and test user details per test environment [${LAB_NAME}]


#### Tests Case in Development = NOT Done

Every tests that it is in progress or needs improvements, those that are NOT DONE, Should be tagged as ```IN_PROGRESS``` using Force Tags.
On Jenkins, We have ```--exclude IN_PROGRESS``` so the Jenkins job will exclude those to be run.

## Installation needed: 
Python 3.9 

Robotframework - 4.1 

Install required packages from required-packages.txt: pip install -r required-packages.txt (NOTE: pyodbc python package installation differs in Windows and Ubuntu)

## How to run Robot Framework
-------------------
Go to robot folder (cd Robot)
#### Run Tests Example:
To run all tests tagged with SANITY
```
python -m robot --loglevel DEBUG -d results -v HEADLESSMODE:true --variable=LAB_NAME:LAB_NL_QA_LTC --variablefile=Resources/stages/conf_test_env.py -i SANITY Tests
```
To run a specific test 
```
python -m robot --loglevel DEBUG -d results -v HEADLESSMODE:true --variable=LAB_NAME:LAB_NL_QA_LTC --variablefile=Resources/stages/conf_test_env.py Tests/01_UMP_Login.robot
```

##### It is also possible to use tags:
-  ```--variable=KEY:VALUE```
-  ```--variablefile=resources/stages/conf_test_env.py```
-  ```--variable=LAB_NAME:LAB_NL_QA_LTC```


##### Suite Setup and Teardown:
- ```Suite Setup       New Browser    ${BROWSER}    headless=${HEADLESSMODE}```
- ```Suite Teardown    Close Browser```


### Contributors
-------------------
 [Shilpa Thorat](https://github.com/ShilpaT26/UMPAutomation)
 

 ### Author
-------------------
##### Shilpa Thorat

