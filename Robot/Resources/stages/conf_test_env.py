#!/usr/bin/env python
#---------------------------------------------------
#------------TEST ENV CONFIG FILE-------------------
#------------LAB_{TEST_ENV} sections----------------
#---------------------------------------------------

LAB_NAME = "LAB_NL_QA_221_LTC"
M365_CONF = {
    "DR_ESSENTIAL_M365_USER": {'username':'admin@M365x859668.onmicrosoft.com', 'password':'jY64hYH4vY'},
    "DR_PLUS_M365_USER": {'username':'admin@M365x859668.onmicrosoft.com', 'password':'jY64hYH4vY'},
    "DR_PRO_M365_USER": {'username':'admin@M365x482361.onmicrosoft.com', 'password':'DZz82s4rcT'},
    "OC_ESSENTIAL_M365_USER": {'username':'admin@M365x289401.onmicrosoft.com', 'password':'aKsy63Q3qM'},
    "OC_PLUS_M365_USER": {'username':'admin@M365x289401.onmicrosoft.com', 'password':'aKsy63Q3qM'},
    "OC_PRO_M365_USER": {'username':'admin@M365x915973.onmicrosoft.com', 'password':'oDOd09j8pX'}
}
ENV_CONF = {
    "LAB_NL_QA_TOBI_LTC": {
        "UMP_SERVER": "ump-tobi.westeurope.cloudapp.azure.com",
        "UMP_DB_INSTANCE": "SQLSYSADMIN",
        "UMP_DB_PORT": "1433",
        "UMP_DB_NAME": "SysAdminTenant",
        "UMP_DB_PWD": "R3m0t3Supp0rt",
        "UMP_ADMIN_CREDENTIALS": {'username':'umpadmin', 'password':'R3m0t3Supp0rt'},
        "BYOC_SBC_CUSTOM_VARS": {'TeamsProxySet':'Teams'},
        "IPPBX_SBC_CUSTOM_VARS": {'IPPBX-proxyaddress':'10.0.0.23', 'IPPBX-proxyaddress-SIPPort':'5060',
                                  'SIP-Hostname':'freepbx.activecommunication.eu'},
        "OVOC_SERVER": "20.86.119.253",
        "OVOC_WIN_SYSADMIN_CREDENTIALS": {'username':'acladmin', 'password':'pass_1234'},
        "SBC_SERVER": "sbc-tobi.westeurope.cloudapp.azure.com",
        "SBC_ADMIN_CREDENTIALS" : {'username':'Admin', 'password':'Admin'},
    },
    "LAB_NL_QA_SHILPA_LTC": {
        "UMP_SERVER": "nl-qa-ump-shilpa.fixedmobileuc.com",
        "UMP_ADMIN_CREDENTIALS": {'username':'umpadmin', 'password':'R3m0t3Supp0rt'},
        "UMP_DB_INSTANCE": "SQLSYSADMIN",
        "UMP_DB_PORT": "1433",
        "UMP_DB_NAME": "SysAdminTenant",
        "UMP_DB_PWD": "R3m0t3Supp0rt",
        "BYOC_SBC_CUSTOM_VARS": {'TeamsProxySet': 'Teams'},
        "IPPBX_SBC_CUSTOM_VARS": {'IPPBX-proxyaddress': '10.0.0.23', 'IPPBX-proxyaddress-SIPPort': '5060',
                                  'SIP-Hostname': 'freepbx.activecommunication.eu'},
        "OVOC_SERVER": "20.86.119.253",
        "OVOC_WIN_SYSADMIN_CREDENTIALS": {'username':'acladmin', 'password':'pass_1234'},
        "SBC_SERVER": "nl-qa-sbc-shilpa.westeurope.cloudapp.azure.com",
        "SBC_ADMIN_CREDENTIALS": {'username':'Admin', 'password':'Admin'},
    },
    "LAB_NL_QA_221_LTC": {
        "UMP_SERVER": "nl-qa-ump-221.fixedmobileuc.com",
        "UMP_ADMIN_CREDENTIALS": {'username':'umpadmin', 'password':'R3m0t3Supp0rt'},
        "UMP_DB_INSTANCE": "SQLSYSADMIN",
        "UMP_DB_PORT": "1433",
        "UMP_DB_NAME": "SysAdminTenant",
        "UMP_DB_PWD": "R3m0t3Supp0rt",
        "BYOC_SBC_CUSTOM_VARS": {'TeamsProxySet': 'Teams'},
        "IPPBX_SBC_CUSTOM_VARS": {'IPPBX-proxyaddress': '10.0.0.23', 'IPPBX-proxyaddress-SIPPort': '5060',
                                  'SIP-Hostname': 'freepbx.activecommunication.eu'},
        "OVOC_SERVER": "20.86.119.253",
        "OVOC_WIN_SYSADMIN_CREDENTIALS": {'username':'acladmin', 'password':'pass_1234'},
        "SBC_SERVER": "nl-qa-sbc-221.westeurope.cloudapp.azure.com",
        "SBC_ADMIN_CREDENTIALS": {'username':'Admin', 'password':'Admin'},
    },
    "LAB_NL_QA06_LTC": {
        "UMP_SERVER": "172.16.5.235",
        "UMP_ADMIN_CREDENTIALS": {'username':'administrator', 'password':'p@ssw0rd'},
        "UMP_DB_INSTANCE": "SQLSYSADMIN",
        "UMP_DB_PORT": "1433",
        "UMP_DB_NAME": "SysAdminTenant",
        "UMP_DB_PWD": "R3m0t3Supp0rt",
        "BYOC_SBC_CUSTOM_VARS": {'TeamsProxySet': 'Teams'},
        "IPPBX_SBC_CUSTOM_VARS": {'IPPBX-proxyaddress': '10.0.0.23', 'IPPBX-proxyaddress-SIPPort': '5060',
                                  'SIP-Hostname': 'freepbx.activecommunication.eu'},
        "OVOC_SERVER": "172.17.239.155",
        "OVOC_WIN_SYSADMIN_CREDENTIALS": {'username':'acladmin', 'password':'pass_1234'},
        "SBC_SERVER": "172.16.5.95", ## In future for UMPs supporting more than 1 SBC, we need to make changes to this variable to multiple values, likewise logic in libraries/keywords consuming this variable needs to be changed.
        "SBC_ADMIN_CREDENTIALS": {'username':'Admin', 'password':'Admin'},
    }
}