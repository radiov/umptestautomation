*** Settings ***
Documentation     A resource file with reusable keywords and variables.
Library           Browser    timeout=30s    retry_assertions_for=30s    enable_playwright_debug=True
Library           Collections
Library           String
Library           OperatingSystem
Library           ../Libraries/Common/SUT.py

*** Variables ***
${browser}        Chromium
${delay}          0

*** Keywords ***
Get Current Page And Context And Browser
    [Documentation]    Get ids for the current context, page and browser in use
    Get Current Page
    Get Current Context
    Get Current Browser

Get Current Page
    [Documentation]    Get id for the current page in use
    ${fetch_page_id}    get page ids    ACTIVE    ACTIVE    ACTIVE
    set suite variable    ${CURRENT_PAGE}    ${fetch_page_id}
    Log    Current page ID is: ${CURRENT_PAGE}

Get Current Context
    [Documentation]    Get id for the current context in use
    ${fetch_context_id}    get context ids    ACTIVE    ACTIVE
    set suite variable    ${CURRENT_CONTEXT}    ${fetch_context_id}
    Log    Current context ID is: ${CURRENT_CONTEXT}

Save Current Context
    [Documentation]    Save current context to continue with any operation with it
    ${state_file}    Save Storage State
    set suite variable    ${CURRENT_CONTEXT_FILE}    ${state_file}

Get Current Browser
    [Documentation]    Get id for the current open browser
    ${fetch_browser_id}    get browser ids    ACTIVE
    set suite variable    ${CURRENT_BROWSER}    ${fetch_browser_id}
    Log    Current browser ID is: ${CURRENT_BROWSER}

Extract ${server} Url
    [Documentation]    Get url for the server specified
    ${url}     get server url    ${server}
    run keyword if    "${url}"!="None"    set suite variable    ${SERVER_URL}    ${url}

Get Value For ${Credentials}
    [Documentation]    Get value for the specified credentials for the server specified
    ${creds}     run keyword if    'M365_USER' not in '${Credentials}'    get server login credentials    ${Credentials}
    ...    ELSE    get m365 login credentials    ${Credentials}
    set suite variable    &{LOGIN_CREDENTIALS}    &{creds}

New Context And New Page For ${server} With ${Credentials}
    [Documentation]    Create a new context and open a new page for specified server with login credentials
    Extract ${server} Url
    Get Value For ${Credentials}
    new context    ignoreHTTPSErrors=true   viewport={'width': 1920, 'height': 1080}    httpCredentials=${LOGIN_CREDENTIALS}
    new page       ${SERVER_URL}
    run keyword if    '${server}' == 'UMP'    Check For Any More Logins Before Accessing UMP Url Content

Check For Any More Logins Before Accessing UMP Url Content
    [Documentation]    Check For Any More Logins Before Accessing UMP Url Content. Sometimes post logging to UMP URl user still sees 2 options "Sign In With AzureAD" "Windows". This keyword is to handle that
    ${source}    get page source
    ${additional_login_found}    run keyword and return status    should contain any    ${source}    Windows    Sign-in with Azure AD
    run keyword if    '${additional_login_found}' == 'True'    click    text=Windows
    run keyword if    '${additional_login_found}' != 'True'    log    No Additional logins to handle. You may proceed.

New Context And New Page For ${server} Without Creds
    [Documentation]    Create a new context and open a new page for specified server without login credentials
    Extract ${server} Url
    new context    ignoreHTTPSErrors=true   viewport={'width': 1920, 'height': 1080}
    new page       ${SERVER_URL}

Current Saved Context And New Page For ${server}
    [Documentation]    Continue with a saved context and open a new page for specified server under saved context
    Extract ${server} Url
    new context    ignoreHTTPSErrors=true    storageState=${CURRENT_CONTEXT_FILE}
    new page    ${SERVER_URL}

