*** Settings ***
Documentation     A resource file with reusable keywords and variables specific to flows origination and ending in SBC only.
Library           ../Libraries/SBC/SBC.py
Library           Collections
Library           String

*** Variables ***

*** Keywords ***
Retrieve SBC Ini Configuration With ${credentials}
    [Documentation]    Fetch all SBC data from device configuration ini file
    Get Value For ${Credentials}
    ${data}    Get All Sbc Data    &{LOGIN_CREDENTIALS}
    ${sbc_data}    convert to string    ${data}
    log    ${sbc_data}
    ${data_exists}    should not be empty    ${sbc_data}
    run keyword if    '${data_exists}'!='0'    set suite variable    ${SBC_INI_DATA}    ${sbc_data}

Get All ${string} Names From SBC
    [Documentation]    Fetch details on all available ${string} provided from SBC device configuration ini file.
    ...   Works only with given strings:
    ...    ProxySet, SIPInterface, DialPlanRule, EtherGroupTable, WebUsers, TLSContexts, AudioCodersGroups,
    ...    IpProfile, CpMediaRealm, SBCRoutingPolicy, SBCAdmissionProfile, SRD, ConditionTable,
    ...    IP2IPRouting, Classification, MessageManipulations, NATTranslation, GwRoutingPolicy
    Retrieve SBC Ini Configuration With SBC_ADMIN_CREDENTIALS
    ${data}    Extract Names From Ini    ${SBC_INI_DATA}    ${string}
    Log    ${data}
    set suite variable    ${SBC-DATA}    ${data}
    log to console    \n Existing Names For ${string} : ${SBC-DATA}

Check IPGroups ${state} In SBC For ${custname} If ${configure_sbc}
    [Documentation]    Run following check only if sbc was programmed. Fetch all ipgroups for provided customer name.
    run keyword if    '${configure_sbc}'=='None'    log to console    \n No IPGroup check to will be done as SBC was not programmed in first place.
    ...    ELSE IF    '${configure_sbc}'=='Configured'    Confirm IPGroups Removed In SBC For ${custname}
    ...    ELSE    Confirm IPGroups Added In SBC For ${custname}

Confirm IPGroups ${value} In SBC For ${custname}
    [Documentation]    Fetch all details on ipgroups created/removed for the provided customer name.
    sleep    6s
    Retrieve SBC Ini Configuration With SBC_ADMIN_CREDENTIALS
    ${data}    Extract Ipgroup Names From Ini    ${SBC_INI_DATA}    ${custname}
    run keyword if    '${value}' == 'Removed' and '''${data}''' != '''${custname} IPGroup not found in Ini File provided'''    Confirm Again If IPGroups Removed In SBC For ${custname}
    ...    ELSE IF    '${value}' == 'Added' and '''${data}''' == '''${custname} IPGroup not found in Ini File provided'''    Confirm Again If IPGroups Added In SBC For ${custname}

Confirm Again If IPGroups ${value} In SBC For ${custname}
    sleep    6s
    Retrieve SBC Ini Configuration With SBC_ADMIN_CREDENTIALS
    ${data}    Extract Ipgroup Names From Ini    ${SBC_INI_DATA}    ${custname}
    run keyword if    '${value}' == 'Removed'    should contain    ${data}    not found
    run keyword if    '${value}' == 'Added'    log to console    \n IPGroups Added In SBC: ${data}

Check ${prefix_added} Prefixes Tags ${state} In SBC For ${sitename} ${dialplanname} If ${configure_sbc}
    [Documentation]    Run following check only if prefixes are added/removed to sbc. Fetch all details on prefixes and tags for provided dial plan name and customer.
    run keyword if    '${configure_sbc}'=='None' or '${prefix_added}'=='None'    log to console    \n No check to will be done in SBC as Prefix, Tag was not added in first place.
    ...    ELSE    Check If Valid Prefixes And Tags Got ${state} In SBC For ${sitename} ${dialplanname}

Get All Prefixes Tags ${state} In SBC For ${sitename} ${dialplanname}
    [Documentation]    Fetch all details on prefixes and tags for provided dial plan name and customer.
    sleep    6s
    Retrieve SBC Ini Configuration With SBC_ADMIN_CREDENTIALS
    ${data}    Extract All Prefix Tag From Ini    ${SBC_INI_DATA}    ${sitename}    ${dialplanname}
    run keyword if    '''${data}''' != '''${dialplanname} and ${sitename} not found in Ini File provided'''    Compare Prefixes And Tags Add In Wizard With SBC Data    ${data}    ${dialplanname}    ${sitename}
    ...    ELSE    log to console    \nPrefixes And Tags From Add Customer Wizard Not Found In SBC After Waiting For 12 seconds.

Check If Valid Prefixes And Tags Got ${state} In SBC For ${sitename} ${dialplanname}
    [Documentation]    Check If Valid Prefixes And Tags Got Added In SBC ${data} For ${sitename} ${dialplanname}
    sleep    6s
    Retrieve SBC Ini Configuration With SBC_ADMIN_CREDENTIALS
    ${data}    Extract All Prefix Tag From Ini    ${SBC_INI_DATA}    ${sitename}    ${dialplanname}
    run keyword if    '${state}' == 'Removed'    should contain    ${data}    not found    Old Prefixes & Tags created still exists: ${data}
    ...    ELSE IF    '${state}' == 'Added' and '''${data}''' == '''${dialplanname} and ${sitename} not found in Ini File provided'''    Get All Prefixes Tags Added In SBC For ${sitename} ${dialplanname}
    ...    ELSE IF    '${state}' == 'Added' and '''${data}''' != '''${dialplanname} and ${sitename} not found in Ini File provided'''    Compare Prefixes And Tags Add In Wizard With SBC Data    ${data}    ${dialplanname}    ${sitename}

Compare Prefixes And Tags Add In Wizard With SBC Data
    [Documentation]    Compare Prefixes And Tags Add In Wizard With SBC Data
    [Arguments]    ${data}    ${dialplanname}    ${sitename}
    FOR    ${each}     IN      @{data}
        should contain any    ${each}    @{SBC_PREFIX_ADDED_IN_ADD_CUSTOMER}    Entered SBC Prefixes @{SBC_PREFIX_ADDED_IN_ADD_CUSTOMER} not found in ${each} of ${dialplanname} for ${sitename}
        should contain    ${each}    ${PSTNGW_ADDED_IN_ADD_CUSTOMER}    msg=Selected GW/Tag ${PSTNGW_ADDED_IN_ADD_CUSTOMER} not found in ${each} of ${dialplanname} for ${sitename}
    END
    log to console    \nPrefixes and Tags added in SBC matches with entries added in Wizard: ${data}