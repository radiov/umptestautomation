*** Settings ***
Documentation     A resource file with reusable keywords and variables specific to flows origination and ending in UMP only.
Library           ../Libraries/UMP/UMPService.py
Library           ../Libraries/UMP/UMPdb.py
Library           Collections
Library           String

*** Variables ***

*** Keywords ***
I Login to UMP And Validate The Login
    [Documentation]    Create a new content and page to login to ump and validate logged in as a valid user.
    Get Title   *=  UMP
    ${logged_user}    get text    body > div.wrapper > nav > ul.navbar-nav.ml-auto > li > div.nav-link    contains    ${LOGIN_CREDENTIALS}[username]
    log to console    \nLogged In As: ${logged_user}

Check For Available Users And Customers
    [Documentation]    Assuming you are logged in to UMP as a valid user get info on available users and customers.
    Get Available Users And Customers

Check For SysAdminKit and MultiTenant Versions
    [Documentation]    Assuming you are logged in to UMP as a valid user get info on installed versions.
    Get SysAdminKit Version
    Get MultiTenant Version

Get Available Users And Customers
    [Documentation]    Extract info on available users and customers assuming user is currently on the tenantui/home/index page
    ${data}    get text    body > div.wrapper > div.content-wrapper > div.content > div:nth-child(3) > div > div > div > div > div:nth-child(5)
    ${details}    get lines containing string    ${data}    Available Users
    @{customer_details}    split string    ${details}    ,
    @{user_details}    split string    ${customer_details}[0]    :
    log to console    \nAvailable Users: ${user_details}[1]
    set suite variable    ${AVAILABLE_USERS}    ${user_details}[1]
    @{cust_details}    split string    ${customer_details}[1]    :
    log to console    \nAvailable Customers: ${cust_details}[1]
    set suite variable    ${AVAILABLE_CUSTOMERS}    ${cust_details}[1]

Get SysAdminKit Version
    [Documentation]    Extract sysadminkit version assuming user is currently on the tenantui/home/index page
    ${data}    get text    body > div.wrapper > div.content-wrapper > div.content > div:nth-child(3) > div > div > div > div > div:nth-child(5)
    ${version_details}    get lines containing string    ${data}    SysadminKit Version
    @{version}    split string    ${version_details}    :
    log to console    \nSysAdminKit Version: ${version}[1]
    set suite variable    ${SYSADMINKIT_VERSION}    ${version}[1]

Get MultiTenant Version
    [Documentation]    Extract multitenant version assuming user is currently on the tenantui/home/index page
    click    body > div.wrapper > aside.main-sidebar.sidebar-dark-primary.elevation-4 > div > nav > ul > li:nth-child(3) > a > p
    click    body > div.wrapper > aside.main-sidebar.sidebar-dark-primary.elevation-4 > div > nav > ul > li.nav-item.menu-open > ul > li:nth-child(1) > a
    ${multitenant_version}    get text    body > div.wrapper > div.content-wrapper > div.content > div > div > div > div > div > div:nth-child(5)
    @{version}    split string    ${multitenant_version}    :
    log to console    \nMultitenant Version: ${version}[1]
    set suite variable    ${MULTITENANT_VERSION}    ${version}[1]

Validate All Items In SideBar Navigation On Home Page
    [Documentation]    Validate the text for all expected sidebar menu and sub-menu items under tenantui/home/index page
    ${first}     get text    body > div.wrapper > aside.main-sidebar.sidebar-dark-primary.elevation-4 > div > nav > ul > li:nth-child(1) > a > p    matches    Tenants
    ${second}     get text    body > div.wrapper > aside.main-sidebar.sidebar-dark-primary.elevation-4 > div > nav > ul > li:nth-child(2) > a > p    ==    Ovoc
    ${second-sub1}     get text    body > div.wrapper > aside.main-sidebar.sidebar-dark-primary.elevation-4 > div > nav > ul > li:nth-child(2) > ul > li:nth-child(1) > a > p    ==    Settings
    ${second-sub2}     get text    body > div.wrapper > aside.main-sidebar.sidebar-dark-primary.elevation-4 > div > nav > ul > li:nth-child(2) > ul > li:nth-child(2) > a > p    ==    Alarms
    ${third}    get text    body > div.wrapper > aside.main-sidebar.sidebar-dark-primary.elevation-4 > div > nav > ul > li:nth-child(3) > a > p    ==    System
    ${third-sub1}    get text    body > div.wrapper > aside.main-sidebar.sidebar-dark-primary.elevation-4 > div > nav > ul > li:nth-child(3) > ul > li:nth-child(1) > a > p    matches    License
    ${third-sub1}    strip string    ${third-sub1}
    ${third-sub2}    get text    body > div.wrapper > aside.main-sidebar.sidebar-dark-primary.elevation-4 > div > nav > ul > li:nth-child(3) > ul > li:nth-child(2) > a > p    ==    Invitation Settings
    ${third-sub3}    get text    body > div.wrapper > aside.main-sidebar.sidebar-dark-primary.elevation-4 > div > nav > ul > li:nth-child(3) > ul > li:nth-child(3) > a > p    ==    Email Settings
    ${third-sub4}    get text    body > div.wrapper > aside.main-sidebar.sidebar-dark-primary.elevation-4 > div > nav > ul > li:nth-child(3) > ul > li:nth-child(4) > a > p    ==    Script Templates
    ${third-sub5}    get text    body > div.wrapper > aside.main-sidebar.sidebar-dark-primary.elevation-4 > div > nav > ul > li:nth-child(3) > ul > li:nth-child(5) > a > p    ==    DNS API Configuration
    ${fourth}    get text    body > div.wrapper > aside.main-sidebar.sidebar-dark-primary.elevation-4 > div > nav > ul > li:nth-child(4) > a > p    ==    Security
    ${fourth-sub1}    get text    body > div.wrapper > aside.main-sidebar.sidebar-dark-primary.elevation-4 > div > nav > ul > li:nth-child(4) > ul > li:nth-child(1) > a > p    ==    Customer Admins
    ${fourth-sub2}    get text    body > div.wrapper > aside.main-sidebar.sidebar-dark-primary.elevation-4 > div > nav > ul > li:nth-child(4) > ul > li:nth-child(2) > a > p    ==    Auth Tokens
    ${fourth-sub3}    get text    body > div.wrapper > aside.main-sidebar.sidebar-dark-primary.elevation-4 > div > nav > ul > li:nth-child(4) > ul > li:nth-child(3) > a > p    ==    Customer Invitations
    ${fifth}    get text    body > div.wrapper > aside.main-sidebar.sidebar-dark-primary.elevation-4 > div > nav > ul > li:nth-child(5) > a > p    ==    SBC List
    ${sixth}    get text    body > div.wrapper > aside.main-sidebar.sidebar-dark-primary.elevation-4 > div > nav > ul > li:nth-child(6) > a > p    ==    Queued Tasks
    log    All Menu Items: ${first}, ${second} [Sub: ${second-sub1}, ${second-sub2}], ${third} [Sub: ${third-sub1}, ${third-sub2}, ${third-sub3}, ${third-sub4}, ${third-sub5}], ${fourth} [Sub: ${fourth-sub1}, ${fourth-sub2}, ${fourth-sub3}], ${fifth}, ${sixth}

Add New Customer With ${license_type} ${fullcustname} ${shortcustname} ${auth_type} ${configure_default_routing} ${configure_sbc} ${enable_carrier_registration} ${enable_cac} ${add_sbc_prefix}
    [Documentation]    Add new customer with chosen license type, full/short customer name and other options from UMP-tenantui/home/index page
    run keyword if    '${license_type}' == 'Essential'    Get Value For DR_ESSENTIAL_M365_USER
    ...    ELSE    run keyword if    '${license_type}' == 'Essential Plus'    Get Value For DR_PLUS_M365_USER
    ...    ELSE    run keyword if    '${license_type}' == 'Essential Pro'    Get Value For DR_PRO_M365_USER
    set test variable    &{m365_creds}    &{LOGIN_CREDENTIALS}
    ${pending_cust_count}    run keyword if    '${license_type}' != 'Essential'    Retrieve Count Of Rows From Table Matching    InvitationId    CustomerInvitation    CustomerId    '${shortcustname}'
    run keyword if    '${license_type}' != 'Essential' and ${pending_cust_count} > 0    Delete Data From Table    CustomerInvitation    CustomerId    '${shortcustname}'
    Click On Plus Icon In UMP To Add New Customer
    Add Customer Wizard Until Configure M365 Default Routing    ${license_type}    ${fullcustname}    ${shortcustname}    ${auth_type}    &{m365_creds}
    Check For Configure M365 Default Routing    ${license_type}    ${configure_default_routing}    &{m365_creds}
    run keyword if    '${configure_sbc}' == 'None'    click    id=nextButton
    ...    ELSE    run keywords    Configure SBC    ${license_type}    ${shortcustname}    ${configure_sbc}    ${add_sbc_prefix}    ${enable_carrier_registration}    ${enable_cac}    &{m365_creds}
    ...    AND    Add SBC Prefix From ${add_sbc_prefix}
    ...    AND    Select SBC Onboarding And Cleanup Scripts For ${configure_sbc}
    Add Customer Wizard Check If Customer Created    ${license_type}    ${fullcustname}    ${shortcustname}
    Confirm If Customer With ${fullcustname} Is Completely Deployed

Click On Plus Icon In UMP To Add New Customer
    [Documentation]    Add new customer with chosen license type and full/short customer name from UMP-tenantui/home/index page
    click    id=iconAdd
    get text    id=addCustomerButton    contains    New Customer

Add Customer Wizard Until Configure M365 Default Routing
    [Documentation]    Add new customer with chosen license type and full/short customer name from UMP-tenantui/home/index page
    [Arguments]    ${license_type}    ${fullname}    ${shortname}    ${auth_type}    &{m365_creds}
    click    id=addCustomerButton
    type text    id=CustomerFullName    ${fullname}             #customer full name
    type text    id=CustomerId    ${shortname}                  #customer short name
    Extract PSTN Gateway    &{m365_creds}
    Select LicenseType    ${license_type}
    run keyword if    '${license_type}' != 'Essential' and '${auth_type}' == 'userpass'    check checkbox    id=useM365Admin
    run keyword if    '${license_type}' != 'Essential' and '${auth_type}' == 'token'    check checkbox    id=useUcAdmin
    run keyword if    '${license_type}' != 'Essential'    Enter Details For Choosen AuthType    ${license_type}    ${auth_type}    ${fullname}    ${shortname}     &{m365_creds}
    run keyword if    '${license_type}' != 'Essential'    If Licensetype Type Is Plus Or Pro Edit M365ConfigurationForm Without CDR And QOE    ${shortname}    &{m365_creds}
    run keyword if    '${license_type}' == 'Essential'    click    id=nextButton

Select LicenseType
    [Documentation]    Choose license type in add new customer wizard
    [Arguments]    ${license_type}    &{m365_creds}
    ${licenseId}    set variable if
    ...    '${license_type}' == 'Essential'    licenseEssential
    ...    '${license_type}' == 'Essential Plus'    licensePro
    ...    '${license_type}' == 'Essential Pro'    licensePremium
    ${status}    get checkbox state    id=${licenseId}
    run keyword if    '${status}' == 'False'    check checkbox    id=${licenseId}

Extract PSTN Gateway
    [Documentation]    Extract pstn gateway from m365 creds.
    [Arguments]    &{m365_creds}
    ${pstngw}    fetch from right    ${m365_creds.username}    @
    set suite variable    ${PSTNGW_ADDED_IN_ADD_CUSTOMER}    ${pstngw}

Enter Details For Choosen AuthType
    [Documentation]    In add new customer wizard select the option to go choosen authentication type
    [Arguments]    ${license_type}    ${auth_type}    ${fullname}    ${shortname}     &{m365_creds}
    run keyword if    '${license_type}' == 'Essential'    click    id=nextButton
    run keyword if    '${license_type}' != 'Essential' and '${auth_type}' == 'userpass'    Enter M365 Cedentails For UserPass Authentication    &{m365_creds}
    run keyword if    '${license_type}' != 'Essential' and '${auth_type}' == 'token'    run keywords    Enter IT Administrator Email For Token Authentication
    ...    AND    Authorize For Token Based On The Link Sent By IT Administrator    ${fullname}    ${shortname}     &{m365_creds}
    ...    AND    should be equal    ${DEVICE_AUTHENTICATED}    ${TRUE}    \nDevice authentication status in DB for ${shortname} is ${DEVICE_AUTHENTICATED}!
    ...    AND    sleep    2s
    ...    AND    New Context And New Page For UMP With UMP_ADMIN_CREDENTIALS
    ...    AND    Click On Plus Icon In UMP To Add New Customer
    ...    AND    click    id=pendingCustomersButton
    ...    AND    Check Status For ${shortname} In Pending Customers And Click Add Customer
    ...    AND    check checkbox    id=licensePremium
    ...    AND    Check For Available User Licenses And Select A Value For LicensedUsersCount In Add Customer Wizard
    ...    AND    click    id=nextButton

Check If ${shortname} In Pending Customers
    [Documentation]    Check StatuCheck If ${shortname} In Pending Customers
    log to console    \nChecking If ${shortname} In Pending Customers. If found click Revoke Request
    ${customers}    get text    id=pendingCustomersList
    ${customer_found}    Get Lines Containing String    ${customers}    ${shortname}
    run keyword if    '${shortname}' in '${customer_found}'    set test variable    ${CUST-IN-PENDING-CUSTOMERS}    ${shortname}
    ...    ELSE    set test variable    ${CUST-IN-PENDING-CUSTOMERS}    ${EMPTY}

Click On Revoke Request For ${shortname}
    [Documentation]    Click On Revoke Request For ${shortname} In Pending customers screen
    log to console    \nFound ${shortname} In Pending Customers. Will Click on Revoke Request.
    ${total_pending_customers}    Retrieve Count Of Rows From Table    CustomerId    CustomerInvitation
    FOR    ${i}    IN RANGE    ${total_pending_customers}
        ${name}    run keyword if    '${total_pending_customers}' == '1'    get text    //*[@id="pendingCustomersList"]/tbody/tr/td[1]
        ...   ELSE    get text    //*[@id="pendingCustomersList"]/tbody/tr[${i}+1]/td[1]
        run keyword if    '${total_pending_customers}' == '1' and '${name}' == '${shortname}'    run keywords    handle future dialogs    action=accept
        ...    AND    click    xpath=//*[@id="pendingCustomersList"]/tbody/tr/td[3]/button[2]
        ...    AND    log    \n${shortname} found in Pending Customer so clicked on Revoke Request For ${name}
        ...    AND    click    id=backbutton
        run keyword if    '${total_pending_customers}' != '1' and '${name}' == '${shortname}'    run keywords    handle future dialogs    action=accept
        ...    AND    click    xpath=//*[@id="pendingCustomersList"]/tbody/tr[${i}+1]/td[3]/button[2]
        ...    AND    log    \n${shortname} found in Pending Customer so clicked on Revoke Request For ${name}
        ...    AND    click    id=backbutton
        Exit For Loop If    '${name}' == '${shortname}'
    END
    Check If ${shortname} In Pending Customers
    should not contain    ${CUST-IN-PENDING-CUSTOMERS}    ${shortname}    \n${shortname} is still visible in Pending Customers!

Check Status For ${shortname} In Pending Customers And Click Add Customer
    [Documentation]    Check Status For a customer In Pending Customers And Click Add Customer
    log to console    \nChecking For Authentication Status in Pending Customers
    ${total_pending_customers}    Retrieve Count Of Rows From Table    CustomerId    CustomerInvitation
    FOR    ${i}    IN RANGE    ${total_pending_customers}
        ${name}    run keyword if    '${total_pending_customers}' == '1'    get text    //*[@id="pendingCustomersList"]/tbody/tr/td[1]
        ${auth_status}    run keyword if    '${total_pending_customers}' == '1' and '${name}' == '${shortname}'    get text    //*[@id="pendingCustomersList"]/tbody/tr/td[2]
        run keyword if    '${total_pending_customers}' == '1' and '${auth_status}' == 'Authentication Complete'    click    //*[@id="pendingCustomersList"]/tbody/tr/td[3]/button[1]
        ...    ELSE    log    \nAuthentication Status For ${name} Is : ${auth_status}
        ${name}    run keyword if    '${total_pending_customers}' != '1'    get text    //*[@id="pendingCustomersList"]/tbody/tr[${i}+1]/td[1]
        ${auth_status}    run keyword if    '${total_pending_customers}' != '1' and '${name}' == '${shortname}'    get text    //*[@id="pendingCustomersList"]/tbody/tr[${i}+1]/td[2]
        run keyword if    '${total_pending_customers}' != '1' and '${auth_status}' == 'Authentication Complete'    click    //*[@id="pendingCustomersList"]/tbody/tr[${i}+1]/td[3]/button[1]
        ...    ELSE    log    \nAuthentication Status For ${name} Is : ${auth_status}
        Exit For Loop If    '${name}' == '${shortname}'
    END

Enter M365 Cedentails For UserPass Authentication
    [Documentation]    In add new customer wizard when option for user and password authentication is selected Enter M365 Cedentails For UserPass Authentication
    [Arguments]    &{m365_creds}
    log to console    \nEntering M365 Cedentails For UserPass Authentication
    type text    id=O365UserName    ${m365_creds.username}
    type text    id=O365Password    ${m365_creds.password}
    Check For Available User Licenses And Select A Value For LicensedUsersCount In Add Customer Wizard
    click    id=nextButton

Enter IT Administrator Email For Token Authentication
    [Documentation]    In add new customer wizard when option for user and password authentication is selected Enter M365 Cedentails For UserPass Authentication
    log to console    \nEntering IT Administrator Email For Token Authentication
    type text    id=AdminEmail    ump-robot@audiocodes.com
    Check For Available User Licenses And Select A Value For LicensedUsersCount In Add Customer Wizard
    click    id=nextButton
    wait until keyword succeeds    1m    5s    get text    id=retList    contains    An email will be sent to this administrator address
    click    id=cancelBtn

Authorize For Token Based On The Link Sent By IT Administrator
    [Documentation]    Follow the process for authorization For Token Based On The Link Sent By IT Administrator
    [Arguments]    ${fullname}    ${shortname}     &{m365_creds}
    #Delete Demo-MS-Teams-PS-Module App From All Enterprise Applications For &{m365_creds} If Available
    ${auth_url}    Retrieve Data From Table    Value    ApplicationSetting    Id    'CustomerAuthenticationPortalUrl'
    ${invitation_id}    Retrieve Data From Table    InvitationId    CustomerInvitation    CustomerId    '${shortname}'
    run keyword if    '${auth_url}' != 'None' and '${invitation_id}' != 'None'    set test variable    ${invitation_url}    ${auth_url}/${invitation_id}
    Start With Token Authentication Process For ${fullname} And ${shortname} On Received ${invitation_url} With &{m365_creds}

Start With Token Authentication Process For ${fullname} And ${shortname} On Received ${invitation_url} With &{m365_creds}
    [Documentation]    Start With Token Authentication Process For ${fullname} On Received ${invitation_url} With ${m365_creds}
    log to console    \nStarting With Token Authentication Process For ${fullname} On Received ${invitation_url}
    new page    ${invitation_url}
    get text    id=main-container    contains    Welcome ${fullname}
    type text    id=email    ${m365_creds.username}
    click    id=main-container
    click    id=btn-submit
    wait until keyword succeeds    20s    2s    get text    id=tail    contains    Device Token Authentication Started
    ${code}    get text    id=copycode
    ${current_page}    Get Page IDs    ACTIVE    ACTIVE    ACTIVE
    set suite variable    ${AUTH_WELCOME_PAGE}    ${current_page}[0]
    Log    Current page ID is: ${AUTH_WELCOME_PAGE}
    Launch Microsoft Device Login Page With ${code} And &{m365_creds} For ${shortname}

Verify Status Of Token Authentication In DB For ${shortname}
    [Documentation]    Verify Status Of Token Authentication In DB
    ${device_authenticated}    Retrieve Data From Table    DeviceAuthenticated    CustomerInvitation    CustomerId    '${shortname}'
    run keyword if    '${device_authenticated}' == 'None'    sleep    2s
    ${auth_status}    run keyword if    '${device_authenticated}' == 'None'    Retrieve Data From Table    DeviceAuthenticated    CustomerInvitation    CustomerId    '${shortname}'
    run keyword if    ${device_authenticated} == ${TRUE}    set suite variable    ${DEVICE_AUTHENTICATED}    ${device_authenticated}
    ...    ELSE    set suite variable    ${DEVICE_AUTHENTICATED}    ${auth_status}

Check For Available User Licenses And Select A Value For LicensedUsersCount In Add Customer Wizard
    [Documentation]    Check For Available User Licenses And Select A Value For LicensedUsersCount In Add Customer Wizard.
    Get Available Users And Customers
    run keyword if    ${AVAILABLE_USERS} >= 1    Enter Licensed Users In Add Customer Wizard
    run keyword if    ${AVAILABLE_USERS} == 0    log to console    \n Not enough User Licenses left to assign to new customer. Free up some licenses OR increase the available user licenses.

Enter Licensed Users In Add Customer Wizard
    [Documentation]    Enter Licensed Users In Add Customer Wizard if available users is greater than 1
    ${users}    Evaluate    random.sample(range(0, ${AVAILABLE_USERS}),1)    random
    Should Not Be Equal    ${users}    0    No User Licenes Available. Cannot proceed further!
    run keyword if    '${users}' != 0    type text    id=licensedUsersCount    ${users}

If Licensetype Type Is Plus Or Pro Edit M365ConfigurationForm Without CDR And QOE
    [Documentation]    If Licensetype Type Is Plus/Pro Check Override admin domain, tenantID, grant admin access.
    [Arguments]    ${shortname}    &{m365_creds}
    wait until keyword succeeds    60s    2s    get text    id=customerId    ==    ${shortname}
    get text    id=O365OverrideAdminDomain    ==    ${PSTNGW_ADDED_IN_ADD_CUSTOMER}
    ${tenantId_prefilled}    get text    id=tenantId
    should not be empty    ${tenantId_prefilled}    Tenant ID Value is missing in M365ConfigurationForm
    type text    id=CustomerTenantAdminAccount    ${m365_creds.username}
    click    id=nextButton

Check For Configure M365 Default Routing
    [Documentation]    Configure M365 Default Routing
    [Arguments]    ${license_type}    ${configure_default_routing}    &{m365_creds}
    run keyword if    '${license_type}' == 'Essential'    log to console    \nM365 Default Routing configuration is not possible for customer with ${license_type} license
    run keyword if    '${license_type}' != 'Essential' and 'configure_default_routing' != 'No'    Configure M365 Default Routing    ${configure_default_routing}    &{m365_creds}
    ...    ELSE    click    id=nextButton

Configure M365 Default Routing
    [Documentation]    Configure M365 Default Routing
    [Arguments]    ${configure_default_routing}    &{m365_creds}
    ${status}    get checkbox state    id=flagConfigureM365Routing
    run keyword if    '${status}' == 'False'    click    text=Configure M365 default routing
    select options by    id=OnlinePstnGateway    value    ${PSTNGW_ADDED_IN_ADD_CUSTOMER}
    click    id=nextButton

Configure SBC
    [Documentation]    Configure SBC based on the options provided
    [Arguments]    ${license_type}    ${shortname}    ${configure_sbc}    ${add_sbc_prefix}    ${enable_carrier_registration}    ${enable_cac}    &{m365_creds}
    get text    id=customerLabel    ==    ${shortname}
    ${status}    get checkbox state    id=flagConfigureSbc
    run keyword if    '${status}' == 'False' and '${license_type}' != 'Essential'   check checkbox    id=flagConfigureSbc
    run keyword if    '${status}' == 'True' and '${license_type}' == 'Essential'   Log to console    \nConfigure SBC is mandatory for ${license_type} Customer
    get text    id=sbcSiteName    ==    ${shortname}
    ${pstn_prefilled}    get text    id=sbcOnlinePstnGateway
    #run keyword if    '${license_type}' == 'Essential'    type text    id=sbcOnlinePstnGateway    ${PSTNGW_ADDED_IN_ADD_CUSTOMER}
    run keyword if    '${pstn_prefilled}' != '${PSTNGW_ADDED_IN_ADD_CUSTOMER}'    type text    id=sbcOnlinePstnGateway    ${PSTNGW_ADDED_IN_ADD_CUSTOMER}
    run keyword if    '${configure_sbc}' == 'SIP'    click    id=SbcConfigurationTypeSipTrunk
    run keyword if    '${configure_sbc}' == 'IPPBX'    click    id=SbcConfigurationTypeIpPbx
    run keyword if    '${configure_sbc}' == 'BYOC'    click    id=SbcConfigurationTypeByoc
    Retrieve SBCs From UMP DB
    Choose An SBC In Add Customer Wizard
    run keyword if    '${configure_sbc}' == 'SIP' or '${configure_sbc}' == 'BYOC'    Select Carrier And Cac For ${configure_sbc} ${enable_carrier_registration} ${enable_cac}
    click    id=nextButton

Select Carrier And Cac For ${configure_sbc} ${enable_carrier_registration} ${enable_cac}
    [Documentation]    Select Carrier And Cac for ${configure_sbc}. Ignore if IPPBX is selected.
    Select A Carrier
    ${carrier}    Evaluate  random.choice(${CARRIER})  random
    select options by    css=#carrierList    label    ${carrier}
    Enable Carrier Registration ${enable_carrier_registration}
    Enable Cac ${enable_cac}

Retrieve SBCs From UMP DB
    [Documentation]    Connect to DB and run a query to fetch list of all added SBCs
    ${data}    Fetch All Data From Table    Sbc
    run keyword if    '${data}' != 'Null'    set suite variable    ${KNOWN_SBCs}    ${data}

Choose An SBC In Add Customer Wizard
    [Documentation]    Connect to DB and run a query to fetch list of all added SBCs and select one from them first preference for OVOC SBC.
    ${known_sbcs}    Evaluate    json.loads('''${KNOWN_SBCs}''')    json
    ${all_sbc_names}    create list
    ${ovoc_sbc_names}    create list
    FOR    ${member}     IN      @{known_sbcs}
        ${name}    Get From Dictionary   ${member}    Name
        append to list    ${all_sbc_names}    ${name}
        ${ovoc_id}    Get From Dictionary   ${member}    OvocSbcId
        run keyword if    '${ovoc_id}' != 'None'    append to list    ${ovoc_sbc_names}    ${name}
    END
    ${no_of_allsbcs}    get length    ${all_sbc_names}
    ${no_of_ovocsbcs}    get length    ${ovoc_sbc_names}
    log to console    \n"${no_of_allsbcs}" SBCs overall configured for this environment : "${all_sbc_names}"!
    log to console    \n"${no_of_ovocsbcs}" OVOC SBCs configured for this environment : "${ovoc_sbc_names}"!
    ${ump_sbc}    run keyword if    ${no_of_allsbcs} > 0    Evaluate  random.choice(${all_sbc_names})  random
    ${ovoc_sbc}    run keyword if    ${no_of_ovocsbcs} > 0    Evaluate  random.choice(${ovoc_sbc_names})  random
    run keyword if    ${no_of_ovocsbcs} > 0    select options by    css=#sbc1    label    ${ovoc_sbc}
    ...    ELSE IF    ${no_of_allsbcs} > 0    select options by    css=#sbc1    label    ${ump_sbc}
    ...    ELSE    log to console    \nCannot select any SBC as there are ${no_of_allsbcs} SBCs available for selection.

Select A Carrier
    [Documentation]    Select A Carrier OR say proxyset from SBC
    Get All ProxySet Names From SBC
    ${proxy-sets}    Set Variable    ${SBC-DATA}
    Get All IpProfile Names From SBC
    ${ip-profiles}    Set Variable    ${SBC-DATA}
    ${carriers}    get valid carriers    ${proxy-sets}    ${ip-profiles}
    ${carriers_exists}    get length    ${carriers}
    run keyword if    ${carriers_exists} > 0    run keywords    Log to console    \nValid carriers:${carriers}
    ...    AND    set suite variable    ${CARRIER}    ${carriers}
    ...    ELSE   Log to console    \nNo Valid carriers found! Check if SBC is configured properly.

Enable Carrier Registration ${enable_carrier_registration}
    [Documentation]    Check carrier registration in add customer wizard and fill in all details.
    run keyword if    '${enable_carrier_registration}' == 'Yes'    click    text=Carrier Registration
    ${checked}    get checkbox state    id=flagCarrierRegistration
    run keyword if    '${checked}' == 'True'    type text    id=carrierUserName    username1
    run keyword if    '${checked}' == 'True'    type text    id=carrierPassword    password1
    run keyword if    '${checked}' == 'True'    type text    id=carrierMainLine    mainline1
    run keyword if    '${checked}' == 'True'    type text    id=carrierHostName    hostname1
    run keyword if    '${enable_carrier_registration}' == 'No'    Log    Carrier Registration is not enabled as per the user's choice.

Enable Cac ${enable_cac}
    [Documentation]    Check Cac in add customer wizard and select Cac Profile.
    run keyword if    '${enable_cac}' == 'No'    Log    \n Cac is not enabled as per the user's choice.
    Select A Cac
    ${cac_profile}    Evaluate  random.choice(${CAC})  random
    run keyword if    '${enable_cac}'=='Yes' and '${cac_profile}'!='NOCAC'    click    text=Enable Cac
    run keyword if    '${enable_cac}'=='Yes' and '${cac_profile}'!='NOCAC'    select options by    css=#CacProfile    label    ${cac_profile}     #write api call OR DB query to retrieve available values
    run keyword if    '${enable_cac}'=='Yes' and '${cac_profile}'=='NOCAC'    Log to console    \n No cac profiles found so skipped enabling cac

Select A Cac
    [Documentation]    Select A Carrier OR say proxyset from SBC
    Get All SBCAdmissionProfile Names From SBC
    ${status}    run keyword and return status    should not contain    ${SBC-DATA}    SBCAdmissionProfile not found
    ${cac_profiles}    Set Variable If
    ...    '${status}'=='True'    ${SBC-DATA}
    ...    '${status}'!='True'    ['NOCAC']
    set suite variable    ${CAC}    ${cac_profiles}

Add SBC Prefix From ${add_sbc_prefix}
    [Documentation]    Add new customer with chosen license type and full/short customer name from UMP-tenantui/home/index page
    run keyword if     '${add_sbc_prefix}'=='File'    Add SBC Prefix From File
    run keyword if     '${add_sbc_prefix}'=='Manual'    Add SBC Prefix Manually
    run keyword if     '${add_sbc_prefix}'=='None'    click    id=nextButton

Add SBC Prefix From File
    [Documentation]    Add new customer with chosen license type and full/short customer name from UMP-tenantui/home/index page
    get text    text=SBC number prefixes
    ${sbcprefix_file}    join path    ${CURDIR}    ../Resources/SBC-Prefixes.csv
    ${promise}    Promise To Upload File    ${sbcprefix_file}
    log    ${promise}
    click    id=customFile
    ${upload_result}    Wait For    ${promise}
    log   ${upload_result}
    get text    id=fileName    contains    SBC     #on successful upload of file
    get text    id=nrs    contains    *** loaded:
    ${rawnumbers}    get text    id=numbers
    ${numbers_string}    Replace String Using Regexp    ${rawnumbers}    \\s+X\\n*    ,
    @{numbers}    Split String    ${numbers_string}    ,
    set suite variable    @{SBC_PREFIX_ADDED_IN_ADD_CUSTOMER}    @{numbers}
    log to console    \n SBC Prefixes added from file: ${SBC_PREFIX_ADDED_IN_ADD_CUSTOMER}
    click    id=nextButton

Add SBC Prefix Manually
    [Documentation]    Add new customer with chosen license type and full/short customer name from UMP-tenantui/home/index page
    get text    text=SBC number prefixes
    ${number}    Generate random string    8    0123456789
    set suite variable    @{SBC_PREFIX_ADDED_IN_ADD_CUSTOMER}    +31${number}
    log to console    \n SBC Prefix added manually: ${SBC_PREFIX_ADDED_IN_ADD_CUSTOMER}
    type text    id=newPhoneNumber    @{SBC_PREFIX_ADDED_IN_ADD_CUSTOMER}
    click    id=iconplus
    click    id=nextButton

Select SBC Onboarding And Cleanup Scripts For ${configure_sbc}
    [Documentation]    Add new customer with chosen license type and full/short customer name from UMP-tenantui/home/index page
    Choose A Proper SBC Script For ${license_type} Customer With ${configure_sbc} Onboarding
    Choose A Proper SBC Script For ${license_type} Customer With ${configure_sbc} Cleanup
    click    id=nextButton

Choose A Proper SBC Script For ${license_type} Customer With ${configure_sbc} ${script_type}
    [Documentation]    Choose A Proper SBC Script For ${license_type} Customer With ${configure_sbc} ${script_type}
    get text    text=SBC ${script_type} Script
    ${script_type_number}     set variable if
    ...    '${script_type}' == 'Onboarding'    1
    ...    '${script_type}' == 'Cleanup'    2
    Log    script_type_number - ${script_type_number}
    ${matchstring}    set variable if
    ...    '${configure_sbc}' == 'SIP'    sbc-scenario7
    ...    '${configure_sbc}' == 'BYOC'    BYOC
    ...    '${configure_sbc}' == 'IPPBX'   IP PBX
    # For below: force to SIP config if config sbc is accidentally set to None in test for Essential customer
    ...    '${configure_sbc}' == 'None' and '${license_type}' == 'Essential'   sbc-scenario7
    Log    matchstring - ${matchstring}
    ${sbc_script_name}    ${custom_vars}    Fetch Sbcscripts Names Customvar Oftype    ${script_type_number}    ${matchstring}
    run keyword if    '${script_type}' == 'Onboarding'    Log to console    \nScript selected: ${sbc_script_name}, Custom vars in db: ${custom_vars}
    ${no_of_customvars}    run keyword if    '${custom_vars}' != 'None'    get length    ${custom_vars}
    ${found}    should not be empty    ${sbc_script_name}    "No sbc onboarding script found Or some issue with DB query"
    run keyword if    '${found}' != '0'    Log    ${sbc_script_name}
    select options by    id=${script_type}Script    text    ${sbc_script_name}
    run keyword if    '${script_type}' == 'Onboarding' and '${no_of_customvars}' != 'None'    Check For ${configure_sbc} Oboarding Script And Enter Values In Wizard For ${custom_vars}

Check For ${configure_sbc} Oboarding Script And Enter Values In Wizard For ${custom_vars}
    [Documentation]     Check For Selected SBC Onboarding Script Custom Vars And Enter Proper Values In Wizard.
    ${config_customvars}    get_sbc_custom_vars_from_config    ${configure_sbc}
    log    ${config_customvars}
    Compare Custom Vars Supplied In ${config_customvars} And One Available In DB ${custom_vars} And Add Values In Wizard

Compare Custom Vars Supplied In ${config_customvars} And One Available In DB ${custom_vars} And Add Values In Wizard
    [Documentation]     Compare Custom Vars Supplied In ${config_customvars} And Available In DB ${custom_vars}
    ${matched_customvars}    compare custom vars from config and db    ${config_customvars}    ${custom_vars}
    log    ${matched_customvars}
    ${no_of_customvars}    get length    ${matched_customvars}
    ${customvar_values}    run keyword if    '${no_of_customvars}' != 'None'    get dictionary values    ${matched_customvars}
    FOR    ${i}    IN RANGE    ${no_of_customvars}
        ${key}    run keyword if    '${no_of_customvars}' == '1'    get text    //*[@id="scriptVariableTable"]/tbody/tr
        run keyword if    '${no_of_customvars}' == '1'    fill text    xpath=//*[@id="scriptVariableTable"]/tbody/tr/td[2]/input    ${customvar_values}[${i}]
        ${key}    run keyword if    '${no_of_customvars}' > '1'    get text    //*[@id="scriptVariableTable"]/tbody/tr[${i}+1]
        run keyword if    '${no_of_customvars}' > '1'     fill text    xpath=//*[@id="scriptVariableTable"]/tbody/tr[${i}+1]/td[2]/input    ${customvar_values}[${i}]
    END

Add Customer Wizard Check If Customer Created
    [Documentation]    Add new customer with chosen license type and full/short customer name from UMP-tenantui/home/index page
    [Arguments]    ${license_type}    ${fullcustname}    ${shortcustname}
    ${status}    wait until keyword succeeds    2m    2s    get text    id=tail    contains    -- CreateCustomer task completed --
    ${errors}    get text    id=tail    not contains    error
    run keyword if    'CreateCustomer task completed' in '''${status}''' and 'error' not in '''${errors}'''   log to console    \nSuccessfully added a new customer with '${license_type}' license type and '${fullcustname}' Full Name And '${shortcustname}' Short Name
    click    id=cancelBtn

Delete UMP Customer With ${fullcustname} ${shortcustname} If It Exists
    [Documentation]    Delete an existing customer with given full customer name from UMP-tenantui/home/index page.
    ...    This is assuming customer was created via UMP.
    log to console    \n Before removal checking if customer with ${fullcustname} and ${shortcustname} was installed, if yes delete it and also ensure IPGroup, Prefixes & Tags are also being removed from SBC
    Search For A Customer With ${fullcustname} In UMP
    ${cust_found}    get variable value    ${CUSTOMER_FOUND}
    run keyword if    '${cust_found}' == 'Customer Not Found'    log    Customer with ${fullcustname} was not found in first row. Please continue!
    ...    ELSE IF    '${cust_found}' == 'Deployed'    Delete ${fullcustname} And ${shortcustname} From UMP And Check It Is Removed From SBC
    ...    ELSE IF    '${cust_found}' != 'Customer Not Found' and '${cust_found}' != 'Deployed'    run keywords    Confirm If Customer With ${fullcustname} Is Deployed
    ...    AND    Delete ${fullcustname} And ${shortcustname} From UMP And Check It Is Removed From SBC

Delete ${fullcustname} And ${shortcustname} From UMP And Check It Is Removed From SBC
    [Documentation]    Delete ${fullcustname} And ${shortcustname} From UMP And Check It Is Removed From SBC
    handle future dialogs    action=accept
    click     text=Undo Deploy
    log    Clicked on Undo Deploy for - ${fullcustname} !!
    Confirm If ${fullcustname} And ${shortcustname} Is Removed From UMP
    Check IPGroups Removed In SBC For ${shortcustname} If Configured
    Check If Valid Prefixes And Tags Got Removed In SBC For ${shortcustname} CustDialPlan

Search For A Customer With ${fullcustname} In UMP
    [Documentation]    Search for a customer with given full customer name from UMP-tenantui/home/index page
    fill text    xpath=//*[@id="TableId_filter"]/label/input    "${fullcustname}"
    ${search_result}    get text    xpath=//*[@id="TableId"]/tbody
    ${cust_found}    run keyword if    '''${search_result}''' != '''No matching records found''' and'''${search_result}''' != '''No data available in table'''   get text    xpath=//*[@id="TableId"]/tbody/tr/td[1]
    ${state}    run keyword if    '''${search_result}''' != '''No matching records found''' and '''${search_result}''' != '''No data available in table'''    get text    xpath=//*[@id="TableId"]/tbody/tr/td[2]
    run keyword if    '${cust_found}' == 'None' and '${state}' == 'None'    set suite variable    ${CUSTOMER_FOUND}    Customer Not Found
    ...    ELSE IF    '${cust_found}' != 'No matching records found' and '${cust_found}' != 'No data available in table' and '${state}' == 'Deployed'     set suite variable    ${CUSTOMER_FOUND}    ${state}
    ...    ELSE IF    '${cust_found}' != 'No matching records found' and '${cust_found}' != 'No data available in table' and '${state}' != 'Deployed'     set suite variable    ${CUSTOMER_FOUND}    ${state}

Confirm If Customer With ${fullcustname} Is ${value}
    [Documentation]    Confirm If Customer With ${fullcustname} Is Completely Added OR Removed from UMP-tenantui/home/index page.
    run keyword if    '${value}' == 'Deploying' or '${value}' == 'Deployed' or '${value}' == 'ReadyForDeployment'    Reload Page To Check If ${fullcustname} State Is Changed To Deployed
    ...    ELSE IF    '${value}' == 'Removed' or '${value}' == 'Removing' or '${value}' == 'ReadyForRemove'    Reload Page To Check If ${fullcustname} Is Removed From UMP Tenants Page
    ...    ELSE IF    '${value}' == 'Customer Not Found'    log to console    \n${fullcustname} is not found on the UMP Tenants Page.
    ...    ELSE IF    '${value}' == 'Error'    run keywords    log to console    \n${fullcustname} is in Error State on the UMP Tenants Page. Will Cleanup this customer!
    ...    AND    Cleanup ${fullcustname} And ${shortcustname} From UMP

Cleanup ${fullcustname} And ${shortcustname} From UMP
    [Documentation]    Cleanup ${fullcustname} And ${shortcustname} From UMP
    handle future dialogs    action=accept
    click     text=Cleanup
    log    Clicked on Cleanup for - ${fullcustname} !!
    Confirm If ${fullcustname} And ${shortcustname} Is Removed From UMP

Confirm If ${fullcustname} And ${shortcustname} Is Removed From UMP
    [Documentation]    Confirm If ${fullcustname} And ${shortcustname} Is Removed From UMP
    Search For A Customer With ${fullcustname} In UMP
    Confirm If Customer With ${fullcustname} Is Removed

Reload Page To Check If ${fullcustname} State Is Changed To ${value}
    [Documentation]    Reload Page To Check ${fullcustname} State from UMP-tenantui/home/index page when customer is created successfully.
    FOR    ${i}    IN RANGE    500
        wait until keyword succeeds    30s    2s    reload
        ${current_state}    get text    xpath=//*[@id="TableId"]/tbody/tr/td[2]
        log    \n${fullcustname} Is Still being Deployed and current state = ${current_state}
        Exit For Loop If    '${current_state}' == '${value}'
    END
    should be equal    ${current_state}    ${value}    Current State: ${current_state} does not match expected state: ${value}

Reload Page To Check If ${fullcustname} Is Removed From UMP Tenants Page
    [Documentation]    Reload Page To Check If ${fullcustname} Is Removed From UMP Tenants Page.
    FOR    ${i}    IN RANGE    450
        wait until keyword succeeds    30s    2s    reload
        ${cust_found}    get text    xpath=//*[@id="TableId"]/tbody/tr/td
        log    \n${fullcustname} Is Still Not Removed and current search returned = ${cust_found}
        Exit For Loop If    '${cust_found}' == 'No matching records found' or '${cust_found}' == 'No data available in table'
    END
    should contain any    ${cust_found}    No matching records found      No data available in table    Customer: ${cust_found} was still not removed!

I Retrieve All Added SBCs In ${server} With ${credentials}
    [Documentation]    API call to retrieve all added SBCs from backend
    Get Value For ${Credentials}
    ${retrieved_sbc_list}     get registered sbc    &{LOGIN_CREDENTIALS}
    log    ${retrieved_sbc_list}
    set suite variable    ${SBC_LIST}    ${retrieved_sbc_list}

Validate Retrieved SBCs
    [Documentation]    Check for the number of sbcs added and print their names
    FOR    ${member}     IN      @{SBC_LIST}
         ${ipAddress}    Get From Dictionary   ${member}     ipAddress
         ${name}    Get From Dictionary   ${member}     name
         ${id}    Get From Dictionary   ${member}     id
         ${ovoc_registered}    Get From Dictionary   ${member}     ovocIdIsValid
         log to console    \nThe sbc ${name} has ip: ${ipAddress} and has following status for registered with OVOC: ${ovoc_registered}
    END

Get All CustomerAdmin From UMP DB
    [Documentation]    Connect to DB and run a query
    ${all_data}    Fetch All Data From Table    CustomerAdmin
    Log To Console    \n${all_data}

