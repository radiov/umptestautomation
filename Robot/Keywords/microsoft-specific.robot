*** Settings ***
Documentation     A resource file with reusable keywords and variables specific to flows origination and ending in UMP only.
Library           Collections
Library           String

*** Variables ***

*** Keywords ***
Check If ${appname} App Is Already Listed Under All Enterprise Applications For &{m365_creds}
    [Documentation]    Check If provied app say "Demo-MS-Teams-PS-Module" App Is Already Listed Under All Enterprise Applications For &{m365_creds}
    new page    https://portal.azure.com/#blade/Microsoft_AAD_IAM/StartboardApplicationsMenuBlade/AllApps/menuId/
    get title    contains    Sign in to Microsoft Azure
    get text    id=loginHeader    contains    Sign in
    type text    id=i0116    ${m365_creds.username}
    click    id=idSIButton9
    get text    id=loginHeader    contains    Enter password
    type text    id=i0118    ${m365_creds.password}
    click    id=idSIButton9
    ${text}    get text    xpath=/html/body/div/form/div
    ${stay_signed_in}    Get Lines Containing String    ${text}    Stay signed in?
    run keyword if    '${stay_signed_in}' == 'Stay signed in?'    click    id=idSIButton9
    get title    contains    Enterprise applications - Microsoft Azure
    click    xpath=//*[@id="1e481a3f-cc9c-4870-85f2-d9a34c863534-infobox-container"]/div[2]/span
    type text    xpath=//*[@id="form-label-id-33-for"]    ${appname}
    ${found}    get text    xpath=//*[@id="_weave_e_1621"]/div/div/div[2]/div/div[2]/div[2]/div/div[3]/span
    run keyword if    '${found}' != '0 applications found'    set test variable    ${APP_FOUND}    ${appname}
    ...    ELSE    set test variable    ${APP_FOUND}    ${found}

Delete ${appname} App From All Enterprise Applications For &{m365_creds} If Available
    [Documentation]    Check If provied app say "Demo-MS-Teams-PS-Module" App Is Already Listed Under All Enterprise Applications For &{m365_creds}. If yes, delete it.
    Check If ${appname} App Is Already Listed Under All Enterprise Applications For &{m365_creds}
    run keyword if    '${APP_FOUND}' == '${appname}'    Delete ${appname} App From All Enterprise Applications For &{m365_creds}
    ...    ELSE    log to console    \n${appname} was not found in All Enterprise Applications For ${m365_creds.username}

Delete ${appname} App From All Enterprise Applications For &{m365_creds}
    [Documentation] Delete ${appname} App From All Enterprise Applications For &{m365_creds}
    click    xpath=//*[@id="grid17787280d-a3ee-4521-87cb-46b98499e1a6"]/div/div/div/div[2]/div[2]/div/table/tbody/tr/td[1]/div/div/div/a
    click    xpath=//*[@id="_weave_e_1913"]/div[2]/div/span/a
    get text    id=7787280d-a3ee-4521-87cb-46b98499e1ce    contains    ${appname}
    get text    id=_weave_e_1782    contains    Properties
    get title    contains    ${appname}
    click    id=_weave_e_1976
    click    id=_weave_e_2039
    Check If ${appname} App Is Already Listed Under All Enterprise Applications For &{m365_creds}
    Should Not Contain    ${APP_FOUND}    ${appname}    ${appname} was not deleted from enterprise applications. Please check what went wrong!

Launch Microsoft Device Login Page With ${code} And &{m365_creds} For ${shortname}
    [Documentation]    Launch Microsoft Device Login Page With ${code} And &{m365_creds}
    ${M365user}    Convert To Lower Case    ${m365_creds.username}
    new page    https://microsoft.com/devicelogin
    log to console    \nLaunched Microsoft Device Login Page
    get text    id=loginHeader    contains    Enter code
    type text    id=otc    ${code}
    click    id=idSIButton9
    ${page_switched}    wait until keyword succeeds    60s    1s    get text    id=loginHeader     not contains    Enter code
    ${loginHeader_text}    get text    id=loginHeader
    run keyword if    '${loginHeader_text}' == 'Pick an account'    Choose ${M365user} From Displayed Microsoft Accounts
    run keyword if    '${loginHeader_text}' == 'Sign in'    run keywords    get text    id=appName    ==    Demo-MS-Teams-PS-Module
    ...    AND    type text    id=i0116    ${m365_creds.username}
    ...    AND    click    id=idSIButton9
    ...    AND    get text    id=loginHeader    contains    Enter password
    ...    AND    type text    id=i0118    ${m365_creds.password}
    ...    AND    click    id=idSIButton9
    ${text}    get text    xpath=/html/body/div/form/div
    ${asking_to_continue}    Get Lines Containing String    ${text}    Are you trying to sign in to Demo-MS-Teams-PS-Module?
    run keyword if    '''${asking_to_continue}''' == '''Are you trying to sign in to Demo-MS-Teams-PS-Module?'''    run keywords    get text    id=appConfirmTitle    contains    Are you trying to sign in to Demo-MS-Teams-PS-Module?
    ...    AND    click    id=idSIButton9
    ...    AND    Continue Without Consent For Token Authentication With M365 Creds    ${shortname}    &{m365_creds}
    ...    ELSE    Check If Consent Is Needed    ${shortname}    &{m365_creds}

Choose ${M365user} From Displayed Microsoft Accounts
    [Documentation]    Choose ${M365user} From Displayed Microsoft Accounts
    ${displayed_accounts}    Get Element Count    xpath=//*[@id="tilesHolder"]/div
    FOR    ${i}    IN RANGE    ${displayed_accounts}
        ${email}    get text    xpath=//*[@id="tilesHolder"]/div[${i}+1]/div/div/div/div[2]/div[2]/small
        run keyword if    '${email}' == '${m365_creds.username}'    click    xpath=//*[@id="tilesHolder"]/div[${i}+1]/div/div/div/div[2]/div[2]/small
        Exit For Loop If    '${email}' == '${m365_creds.username}'
    END

Check If Consent Is Needed
    [Documentation]    Check If Consent Is Needed
    [Arguments]    ${shortname}    &{m365_creds}
    ${consents_needed}    get text    id=consentHeader
    run keyword if    '${consents_needed}' == 'Permissions requested'    Consent For Token Authentication With M365 Creds    ${shortname}    &{m365_creds}
    ...    ELSE    Continue Without Consent For Token Authentication With M365 Creds    ${shortname}    &{m365_creds}

Continue Without Consent For Token Authentication With M365 Creds
    [Documentation]    Continue Without Consent For Token Authentication With &{m365_creds}
    [Arguments]    ${shortname}    &{m365_creds}
    log to console    \nContinuing Without Consents For Token Authentication.
    get text    id=message    contains    You have signed in to the Demo-MS-Teams-PS-Module application on your device. You may now close this window
    switch page    ${AUTH_WELCOME_PAGE}
    #sleep    4s
    ${tail_text}    wait until keyword succeeds    30s    2s    get text    id=tailContainer    contains    Device Token Authentication Started
    #${auth_started}    get text    xpath=//*[@id="tail"]/div[1]
    #${auth_process}    get text    xpath=//*[@id="tail"]/div[4]
    ${authorize_again}    Get Lines Containing String    ${tail_text}    Click here to continue the authentication process
    run keyword if    '''${authorize_again}''' == '''Click here to continue the authentication process'''    Give Second Consent For Token Auth With M365 Creds    &{m365_creds}
    ...    ELSE    get text    xpath=//*[@id="tail"]/div[7]/span    contains    Authentication OK
    Verify Status Of Token Authentication In DB For ${shortname}

Consent For Token Authentication With M365 Creds
    [Documentation]    Consent For Token Authentication
    [Arguments]    ${shortname}    &{m365_creds}
    log to console    \nStarting With First Consent For Token Authentication With M365 Creds.
    Give First Consent For Token Auth With M365 Creds    &{m365_creds}
    log to console    \nStarting With Second Consent For Token Authentication With M365 Creds.
    Give Second Consent For Token Auth With M365 Creds    &{m365_creds}
    Verify Status Of Token Authentication In DB For ${shortname}

Give First Consent For Token Auth With M365 Creds
    [Documentation]    Give First Consent For Token Auth With M365 Creds    &{m365_creds}
    [Arguments]    &{m365_creds}
    check checkbox    id=upgradeConsentCheckbox
    click    id=idSIButton9
    get text    id=message    contains    You have signed in to the Demo-MS-Teams-PS-Module application on your device. You may now close this window
    switch page    ${AUTH_WELCOME_PAGE}
    wait until keyword succeeds    30s    2s    get text    xpath=//*[@id="tail"]/div[3]/span    contains    Graph Token - Completed

Give Second Consent For Token Auth With M365 Creds
    [Documentation]    Give Second Consent For Token Auth With M365 Creds    &{m365_creds}
    [Arguments]    &{m365_creds}
    ${url}    get property    xpath=//*[@id="tail"]/div[4]/a    href
    click    text=Click here to continue the authentication process
    go to    ${url}
    ${title}    get text    id=loginHeader
    ${M365user}    Convert To Lower Case    ${m365_creds.username}
    run keyword if    '${title}' == 'Sign in'    run keywords    type text    id=i0116    ${m365_creds.username}
    ...    AND    click    id=idSIButton9
    ...    AND    get text    id=displayName    contains    ${M365user}
    ...    AND    type text    id=i0118    ${m365_creds.password}
    ...    AND    click    id=idSIButton9
    run keyword if    '${title}' == 'Pick an account'    Choose ${M365user} From Displayed Microsoft Accounts
    get text    id=consentHeader    contains    Permissions requested
    check checkbox    id=upgradeConsentCheckbox
    click    id=idSIButton9
    get text    xpath=//*[@id="main-container"]/div[2]/p[1]    contains    Thank you
    log to console    \nAuthentication was successful!!