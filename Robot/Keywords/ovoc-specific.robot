*** Settings ***
Documentation     A resource file with reusable keywords and variables specific to flows origination and ending in UMP only.
Library           ../Libraries/OVOC/keywords.py

*** Variables ***

*** Keywords ***
Login To OVOC With ${Credentials} User
    [Documentation]    Login to OVOC with given user type and validate that logged in as correct user
    Get Value For ${Credentials}
    get text    body > app-root > div > login > ac-splash-screen > div > div.splash-content-container > div > div.title    ==    Login
    type text    id=login-username    ${LOGIN_CREDENTIALS}[username]
    ${azure_check}    get checkbox state    id=isAzureAuthentication
    sleep    2s
    run keyword if    '${azure_check}' == 'True'    uncheck checkbox    id=isAzureAuthentication
    sleep    2s
    get element state    id=login-password    disabled    !=    true
    type text    id=login-password    ${LOGIN_CREDENTIALS}[password]
    click    body > app-root > div > login > ac-splash-screen > div > div.splash-content-container > div > login-form > div > span > ac-button    force=True
    Get Title   *=  Live
    ${logging_details}    get text    id=user-menu-toggle    contains    ${LOGIN_CREDENTIALS}[username]
    @{logged_user}    split string    ${logging_details}    \n
    log to console    \nLogged In As: ${logged_user}[1]

Get OVOC Version
    [Documentation]    Extract ovoc version assuming user is currently in to ovoc
    click    xpath=//*[@id="user-menu-toggle"]/ac-svg/div
    get text    xpath=//*[@id="user-menu-content"]/ac-menu-options/div/div[2]/div/div[2]/div[2]/span    ==    About
    click    xpath=//*[@id="user-menu-content"]/ac-menu-options/div/div[2]/div/div[2]/div[2]/span
    get text    id=about-dialog    contains    ABOUT
    ${listed_version}    get text    xpath=//*[@id="about-dialog"]/div[2]/about-dialog/div/div[2]/div/div[1]
    @{version}    split string    ${listed_version}
    log to console    \nOVOC Version: ${version}[1]
    set suite variable    ${OVOC_VERSION}    ${version}[1]

