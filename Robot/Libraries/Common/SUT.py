#!/usr/bin/env python39
"""
This module contains the class which gives information
about the system under test based on the LAB_NAME from conf_test_env.py
"""
import operator
import sys
from robot.libraries.BuiltIn import BuiltIn, RobotNotRunningError

class SUT(object):
    """
    Evaluate type of system under test and extract information on servers used.
    """
    def __init__(self):
        """
        Initialise.
        Check if proper labname is passed for execution. Should contain either LTC OR Standalone.
        """
        try:
            self.lab_name = BuiltIn().get_variable_value("${LAB_NAME}")
            if self.lab_name:
                if operator.contains(self.lab_name, "LTC"):
                    print("System under test is LTC (Live Teams CLoud)!")
                    self.ovoc_support = "Enabled"
                elif operator.contains(self.lab_name, "STANDALONE"):
                    print("System under test is Standalone (No OVOC support)!")
                    self.ovoc_support = "Disabled"
                else:
                    BuiltIn().log_to_console(
                        "ERROR: : %s neither identified as LTC OR STANDALONE. FIX YOUR LAB_NAME \n" % self.lab_name)
                    sys.exit()
            else:
                BuiltIn().log_to_console("WARN: LAB_NAME is empty - LAB_NAME:%s" % self.lab_name)
        except RobotNotRunningError:
            pass

    def get_server_url(self, server_name):
        """
        method to get server name from lab name
        :param server_name: name of the server for which url needs to be returned can be UMP, OVOC OR SBC
        :return response: server url returned
        """
        try:
            server = BuiltIn().get_variable_value(
                    "${ENV_CONF['" + self.lab_name + "']['" + server_name + "_SERVER']}")
            if server_name == "UMP":
                server_url = "https://"+server+"/tenantui/Home/Index"
            elif server_name == "OVOC" and self.ovoc_support == "Enabled":
                server_url = "https://"+server+"/web-ui-ovoc/login"
            elif server_name == "OVOC" and self.ovoc_support == "Disabled":
                BuiltIn().log_to_console("OVOC Specific Tests Not Supported On This System!")
                server_url = "None"
            elif server_name == "SBC":
                server_url = "https://"+server+"/"
            return server_url
        except RobotNotRunningError:
            pass

    def get_server_login_credentials(self, creds_var):
        """
        method to get server login credentials
        :param creds_var: name of the credential variable from which credentials needs to be extracted
        :return response: server credentials returned as dictionary
        """
        try:
            credentials = BuiltIn().get_variable_value(
                    "${ENV_CONF['" + self.lab_name + "']['" + creds_var + "']}")
            if credentials is None:
                BuiltIn().log_to_console(
                    "ERROR: : %s doesn't exist. Please check if this was added to the %s config file. \n" % (creds_var, self.lab_name))
                credentials = "ERROR: Cannot retrieve value for the variable as it is not specified in config"
            else:
                credentials = credentials
                return credentials
        except RobotNotRunningError:
            pass

    def get_m365_login_credentials(self, creds_var):
        """
        method to get m365 login credentials
        :param creds_var: name of the credential variable from which credentials needs to be extracted
        :return response: appropriate m365 credentials returned as dictionary
        """
        try:
            credentials = BuiltIn().get_variable_value(
                    "${M365_CONF['" + creds_var + "']}")
            if credentials is None:
                BuiltIn().log_to_console(
                    "ERROR: : %s doesn't exist. Please check if this was added to the %s config file. \n" % (creds_var, self.lab_name))
                credentials = "ERROR: Cannot retrieve value for the variable as it is not specified in config"
            else:
                credentials = credentials
                return credentials
        except RobotNotRunningError:
            pass

    def get_sbc_custom_vars_from_config(self, sbcconfig):
        """
        method to get custom vars from DB and match then with names and values from conf_test_env.py and then extract
        the values for each custom variable for the selected sbc onboarding script type
        :param sbcconfig: name of the choosen sbc config can be SIP, BYOC OR IPPBX
        :return response: custom variables values from conf_test_env.py
        """
        try:
            customvars = BuiltIn().get_variable_value(
                    "${ENV_CONF['" + self.lab_name + "']['" + sbcconfig + "_SBC_CUSTOM_VARS']}")
            if customvars is None:
                BuiltIn().log_to_console(
                    "ERROR: : %s doesn't exist. Please check if this was added to the %s config file. \n" % (customvars, self.lab_name))
                customvars = "ERROR: Cannot retrieve value for the variable as it is not specified in config"
            else:
                print(customvars.items())
                return customvars
        except RobotNotRunningError:
            pass

    def compare_custom_vars_from_config_and_db(self, configvars, dbvars):
        """
        method to get compare custom vars supplied in config file and what's available in DB for the selected sbc onboarding script type
        :param configvars, dbvars
        :return response: return configvars if all keys found in dbvars else error out to correct the config file as per db.
        """
        try:
            dbvar_list = dbvars.split(',')
            count_dbvars = len(dbvar_list)
            count_configvars = len(configvars)
            matched_customvars = {}
            if count_dbvars == count_configvars:
                BuiltIn().log_to_console("Custom Variables count supplied in config file matches with the Custom Variables count supplied in DB")
                print(dbvar_list)
                for item in dbvar_list:
                    print(item)
                    for k, v in configvars.items():
                        if item == k:
                            value = v
                            print(value)
                            matched_customvars[k] = v
                            print(matched_customvars)
                return matched_customvars
            else:
                BuiltIn().log_to_console(
                    "Custom Variables count supplied in config file matches with the Custom Variables count supplied in DB")
                return matched_customvars
        except RobotNotRunningError:
            pass