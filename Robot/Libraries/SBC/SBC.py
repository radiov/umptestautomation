#!/usr/bin/env python39
# -*- coding: utf-8 -*-
"""
Description         Class definition for the SBC Rest APIs
"""

import re
import requests
from requests.auth import HTTPBasicAuth
from requests import HTTPError
from requests.packages.urllib3.exceptions import InsecureRequestWarning
from robot.libraries.BuiltIn import BuiltIn, RobotNotRunningError
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


class SBC(object):
    """
    Reference: https://activecommunications.atlassian.net/wiki/spaces/UMP/pages/624525328/SBC+integration+Programming
               ?preview=/624525328/733806629/rest-api-for-mediant-devices-ver-72.pdf
    """
    def __init__(self):
        """
        Initialise.
        Based on the labname passed for execution build the url for SBC server.
        """
        try:
            self.lab_name = BuiltIn().get_variable_value("${LAB_NAME}")
            self.server = BuiltIn().get_variable_value(
                "${ENV_CONF['" + self.lab_name + "']['SBC_SERVER']}")
            if self.server:
                self.sbc_url = "https://" + self.server
            else:
                BuiltIn().log_to_console("WARN: SBC_SERVER is not provided under - LAB_NAME:%s" % self.lab_name)
        except RobotNotRunningError:
            pass

    def get_all_sbc_data(self, **credentials):
        """
        The INI file is the main device configuration file.
        URL: /api/v1/files/ini
        """
        try:
            url = self.sbc_url + "/api/v1/files/ini"
            parameters = ""
            headers = {
                'content-type': 'application/octet-stream'
            }
            username = credentials["username"]
            password = credentials["password"]
            response = requests.get(url, headers=headers, params=parameters,
                                    auth=HTTPBasicAuth(username, password), verify=False, timeout=10)
            if response.status_code in [200]:
                return response.content
            else:
                raise HTTPError('Status:', response.status_code,
                                response.reason)
        except RobotNotRunningError:
            pass

    def extract_names_from_ini(self, inicontent, string):
        """
        Extract specific type of data from the INI file:
        works only for below:
        ProxySet, SIPInterface, DialPlanRule, EtherGroupTable, WebUsers, TLSContexts, AudioCodersGroups,
        IpProfile, CpMediaRealm, SBCRoutingPolicy, SBCAdmissionProfile, SRD, ConditionTable,
        IP2IPRouting, Classification, MessageManipulations, NATTranslation, GwRoutingPolicy
        """
        try:
            lines = inicontent
            searchstring = string
            # below reg exp pattern is valid only for the strings mentioned in the documentation of this method
            search_regex = rf'{searchstring} [0-9]+ = "[a-zA-Z0-9]+\s*[_]*[-]*[a-zA-Z0-9]*"'
            match = re.findall(search_regex, lines)
            print(match)
            if match:
                newlist = [re.sub('[" ]', '', item) for item in match]   # remove whitespaces and "" from the list
                data_set = [s.split('=')[1] for s in newlist]    # split list based on = delimiter to get the values for 'string' names
                final_data_set = []
                for i in data_set:
                    if i not in final_data_set:
                        final_data_set.append(i)
                return final_data_set
            else:
                return "%s not found in Ini File provided" % string
        except RobotNotRunningError:
            pass

    def get_valid_carriers(self, list1, list2):
        """Inputs provided are the proxysets and ipprofiles. In this method we need to return only those
        values which are present in both lists but need to exclude Teams.
        """
        carriers = []
        for value in list1:
            if value in list2:
                carriers.append(value)
        carriers.remove('Teams')
        return carriers

    def extract_all_prefix_tag_from_ini(self, inicontent, sitename, dialplan):
        """
        Extract all prefixes and tags for provided customer name and dial plan rule name from ini file
        """
        try:
            search_regex = rf'DialPlanRule [0-9]+ = "{dialplan}", [0-9]+, "{sitename}", ".*", ".*"'
            match = re.findall(search_regex, inicontent)
            print(match)
            if match:
                newlist = [re.sub('[" ]', '', item) for item in match]  # remove whitespaces and "" from the list
                data_set = [s.split(',') for s in newlist]
                print(data_set)
                return data_set
            else:
                return "%s and %s not found in Ini File provided" % (dialplan, sitename)
        except RobotNotRunningError:
            pass

    def extract_ipgroup_names_from_ini(self, inicontent, custname):
        """
        Extract all created ipgroup names for provided customer name from ini file
        """
        try:
            search_regex = rf'IPGroup [0-9]+ = [0-9]+, "{custname}-[ct]"'
            match = re.findall(search_regex, inicontent)
            print(match)
            if match:
                newlist = [re.sub('[" ]', '', item) for item in match]  # remove whitespaces and "" from the list
                data_set = [s.split(',')[1] for s in newlist]
                print(data_set)
                return data_set
            else:
                return "%s IPGroup not found in Ini File provided" % custname
        except RobotNotRunningError:
            pass