#!/usr/bin/env python39
# -*- coding: utf-8 -*-
"""
Description         Class definition for the UMP APIs
Reference: https://UMP_SERVER/tenantui/swagger/index.html
"""

import requests
from requests import HTTPError
from requests_ntlm import HttpNtlmAuth
from requests.packages.urllib3.exceptions import InsecureRequestWarning
from robot.libraries.BuiltIn import BuiltIn, RobotNotRunningError
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)



class UMPService(object):
    """
    Reference: https://UMP_SERVER/tenantui/swagger/index.html
    """
    def __init__(self):
        """
        Initialise.
        Based on the labname passed for execution build the url for UMP server.
        """
        try:
            self.lab_name = BuiltIn().get_variable_value("${LAB_NAME}")
            self.server = BuiltIn().get_variable_value(
                "${ENV_CONF['" + self.lab_name + "']['UMP_SERVER']}")
            if self.server:
                self.ump_url = "https://" + self.server
            else:
                BuiltIn().log_to_console("WARN: UMP_SERVER is not provided under - LAB_NAME:%s" % self.lab_name)
        except RobotNotRunningError:
            pass

    def get_registered_sbc(self, **credentials):
        """
        http get sbc request
        :param url: The request url
        :return response: The response from the request
        """
        try:
            url = self.ump_url + "/tenantui/api/sbc"
            parameters = ""
            headers = {
                'content-type': 'application/json'
            }
            username = credentials["username"]
            password = credentials["password"]
            response = requests.get(url, headers=headers, params=parameters,
                                    auth=HttpNtlmAuth(username, password), verify=False, timeout=10)
            if response.status_code in [200, 204]:
                return response.json()
            else:
                raise HTTPError('Status:', response.status_code,
                                response.reason)
        except RobotNotRunningError:
            pass

