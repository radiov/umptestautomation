#!/usr/bin/env python39
# -*- coding: utf-8 -*-
"""
Description         Class definition for the UMP DB Queries
Reference: Wiki page for SQL queries to be added once it's created.
"""

import pyodbc
import json
from datetime import date, datetime
from robot.libraries.BuiltIn import BuiltIn, RobotNotRunningError

json_serialize = lambda obj: (obj.isoformat() if isinstance(obj, (datetime, date)) else None)

class UMPdb(object):
    """
    Reference: Wiki page for SQL queries to be added once it's created.
    """
    def __init__(self):
        """
        Initialise.
        Build db connection string for the UMP server id DB details being passed in the LAB_NAME
        :return response: db connection string returned
        """
        try:
            self.lab_name = BuiltIn().get_variable_value("${LAB_NAME}")
            self.server = BuiltIn().get_variable_value(
                "${ENV_CONF['" + self.lab_name + "']['UMP_SERVER']}")
            self.db_instance = BuiltIn().get_variable_value(
                    "${ENV_CONF['" + self.lab_name + "']['UMP_DB_INSTANCE']}")
            self.db_port = BuiltIn().get_variable_value(
                "${ENV_CONF['" + self.lab_name + "']['UMP_DB_PORT']}")
            self.db_name = BuiltIn().get_variable_value(
                "${ENV_CONF['" + self.lab_name + "']['UMP_DB_NAME']}")
            self.db_password = BuiltIn().get_variable_value(
                "${ENV_CONF['" + self.lab_name + "']['UMP_DB_PWD']}")
            if None in [self.db_instance, self.db_port, self.db_name, self.db_password]:
                BuiltIn().log_to_console(
                    "ERROR: : Expected DB parameters doesn't exist. Please check if this was added to the %s "
                    "config file. \n" % self.lab_name)
            else:
                pass
        except RobotNotRunningError:
            pass

    def connectDB(self):
        '''
        method to fetch connection string for UMP and connect to DB using it.
        :return sql server db connection.
        '''
        try:
            connection = pyodbc.connect('Driver={ODBC Driver 17 for SQL Server};''Server=' + self.server + '\\' + self.db_instance + ','
                                        + self.db_port + ';''Database=' + self.db_name + ';''UID=sa;''PWD=' + self.db_password + ';')
            return connection
        except pyodbc.ProgrammingError:
            pass

    def closeDB(self, connection):
        '''Take input connection and close the database connection'''
        try:
            connection.close()
        except pyodbc.ProgrammingError:
            pass

    def convert_to_json(self, row_headers, data):
        '''Convert sql query results to json format'''
        json_data = []
        for result in data:
            json_data.append(dict(zip(row_headers, result)))
        return json_data

    def fetch_all_data_from_table(self, table):
        """
        method to fetch all data for any table from UMP DB.
        :return table contents.
        """
        try:
            connection = self.connectDB()
            cursor = connection.cursor()    # Initialise the Cursor
            sql = "SELECT * from %s" % table
            cursor.execute(sql)
            row_headers = [x[0] for x in cursor.description]
            data = cursor.fetchall()
            json_data = self.convert_to_json(row_headers, data)
            cursor.close()
            self.closeDB(connection)
            return json.dumps(json_data, default=json_serialize)
        except RobotNotRunningError:
            pass

    def fetch_sbcscripts_names_customvar_oftype(self, scripttype, matchstring):
        """
        method to fetch all sbc scripts for a specific type (1 - sbc onboarding,2 - sbc cleanup,
        3 - m365 onboarding ,4 - m365 cleanup) from UMP DB.
        :return list of the SBCs.
        """
        try:
            match = '%'+matchstring+'%'
            connection = self.connectDB()
            cursor = connection.cursor()    # Initialise the Cursor
            sql = "SELECT FriendlyName, CustomerVariables from SbcScriptTemplate where ScriptType=? and FriendlyName like '%s'" % match
            params = [scripttype]
            cursor.execute(sql, params)
            row = cursor.fetchone()
            print(row)
            if row:
                print(row[0], row[1])
            else:
                if scripttype == '1':
                    cursor.execute("SELECT FriendlyName, CustomerVariables from SbcScriptTemplate where ScriptType=? "
                               "and FriendlyName in ('sbc-scenario7')", scripttype)        #fall back to the default onbaording script
                    row = cursor.fetchone()
                if scripttype == '2':
                    cursor.execute("SELECT FriendlyName, CustomerVariables from SbcScriptTemplate where ScriptType=? "
                               "and FriendlyName in ('sbc-scenario7Cleanup')", scripttype) #fall back to the default cleanup script
                    row = cursor.fetchone()
            cursor.close()
            self.closeDB(connection)
            return row[0], row[1]
        except RobotNotRunningError:
            pass

    def retrieve_data_from_table(self, column, table, property, name):
        """
        method to fetch data, count for any specific property matching a name from the database table of UMP DB.
        :return value matching the property and name from that table plus total count for the match.
        """
        try:
            connection = self.connectDB()
            cursor = connection.cursor()  # Initialise the Cursor
            sql = "SELECT %s from %s where %s = %s" % (column, table, property, name)
            cursor.execute(sql)   # Executing a SQL Query
            data = cursor.fetchone()
            cursor.close()
            self.closeDB(connection)
            return data[0]
        except RobotNotRunningError:
            pass

    def retrieve_count_of_rows_from_table(self, column, table):
        """
        method to return count of total rows for a specific property from any database table of UMP DB.
        :return count of rows
        """
        try:
            connection = self.connectDB()
            cursor = connection.cursor()  # Initialise the Cursor
            sql = "SELECT COUNT(%s) from %s" % (column, table)
            cursor.execute(sql)   # Executing a SQL Query
            data = cursor.fetchone()
            cursor.close()
            self.closeDB(connection)
            return data[0]
        except RobotNotRunningError:
            pass

    def retrieve_count_of_rows_from_table_matching (self, column, table, property, name):
        """
        method to return count of total rows for a specific property matching specific name from any database table of UMP DB.
        :return count of rows
        """
        try:
            connection = self.connectDB()
            cursor = connection.cursor()  # Initialise the Cursor
            sql = "SELECT COUNT(%s) from %s where %s = %s" % (column, table, property, name)
            cursor.execute(sql)   # Executing a SQL Query
            data = cursor.fetchone()
            cursor.close()
            self.closeDB(connection)
            return data[0]
        except RobotNotRunningError:
            pass

    def delete_data_from_table(self, table, property, name):
        """
        method to fetch delete for any specific row matching property with specific name from the database table of UMP DB.
        :return entry removed from that table.
        """
        try:
            connection = self.connectDB()
            cursor = connection.cursor()  # Initialise the Cursor
            sql = "DELETE FROM %s where %s = %s" % (table, property, name)
            cursor.execute(sql)   # Executing a SQL Query
            cursor.commit()
            cursor.close()
            self.closeDB(connection)
        except RobotNotRunningError:
            pass