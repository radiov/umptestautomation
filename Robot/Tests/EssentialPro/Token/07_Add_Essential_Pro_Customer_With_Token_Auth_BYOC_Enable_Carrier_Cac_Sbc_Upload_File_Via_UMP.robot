*** Settings ***
Suite Setup       New Browser    ${BROWSER}    headless=${HEADLESSMODE}
Suite Teardown    Close Browser
Force Tags        SANITY    ESSENTIALPRO    DONE
Resource          ./settings.robot

#Author     Shilpa Thorat

*** Variables ***
${license_type}                     Essential Pro                            # Supported options: Essential or Essential Plus or Essential Pro
${fullcustname}                     Auto ${license_type} ${configure_sbc} Customer
${shortcustname}                    AutoProC${configure_sbc}
${auth_type}                        token                                  # Supported options: userpass OR token OR none
${configure_default_routing}        Yes                                       # Supported options: No or Yes
${configure_sbc}                    BYOC                                       # Supported options: SIP or BYOC or IPPBX(only for plus and pro) or None
${enable_carrier_registration}      Yes                                       # Supported options: No or Yes
${enable_cac}                       Yes                                       # Supported options: No or Yes
${add_sbc_prefix}                   File                                      # Supported options: File, Manual, None

*** Test Cases ***
Add New ${fullcustname} Customer With ${license_type} License If It Does Not Exists
    Given New Context And New Page For UMP With UMP_ADMIN_CREDENTIALS
    When Delete UMP Customer With ${fullcustname} ${shortcustname} If It Exists
    Then Add New Customer With ${license_type} ${fullcustname} ${shortcustname} ${auth_type} ${configure_default_routing} ${configure_sbc} ${enable_carrier_registration} ${enable_cac} ${add_sbc_prefix}
    AND Search For A Customer With ${fullcustname} In UMP
    AND Confirm If Customer With ${fullcustname} Is Deployed
    AND Check IPGroups Added In SBC For ${shortcustname} If ${configure_sbc}
    AND Check ${add_sbc_prefix} Prefixes Tags Added In SBC For ${shortcustname} CustDialPlan If ${configure_sbc}