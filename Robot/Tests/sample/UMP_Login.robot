*** Settings ***
Suite Setup       New Browser    ${BROWSER}    headless=${HEADLESSMODE}
Suite Teardown    Close Browser
Force Tags        SAMPLE    INPROGRESS
Resource          ./settings.robot

#Author     Shilpa Thorat

*** Variables ***

*** Test Cases ***
Test For Valid Login To UMP And Check For All Menu And Sub-Menu Items In UMP Home Page
    Given New Context And New Page For UMP With UMP_ADMIN_CREDENTIALS
    AND I Login to UMP And Validate The Login
    When Check For Available Users And Customers
    And Check For SysAdminKit and MultiTenant Versions
    Then Validate All Items In SideBar Navigation On Home Page


