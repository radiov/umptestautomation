*** Settings ***
Suite Setup       New Browser    ${BROWSER}    headless=${HEADLESSMODE}
Suite Teardown    Close Browser
Force Tags        SAMPLE    INPROGRESS
Resource          ./settings.robot

##Author     Shilpa Thorat

*** Test Cases ***
Test To Retrieve SBC Ini Configuration
    Given Retrieve SBC Ini Configuration With SBC_ADMIN_CREDENTIALS
    When Get All ProxySet From SBC
    AND Get All SIPInterface From SBC
    AND Get All SBCAdmissionProfile From SBC
    AND Get All IpProfile From SBC
    AND Get All EtherGroupTable From SBC
    AND Get All WebUsers From SBC
    AND Get All TLSContexts From SBC
    AND Get All AudioCodersGroups From SBC
    AND Get All CpMediaRealm From SBC
    AND Get All SBCRoutingPolicy From SBC
    AND Get All SRD From SBC
    AND Get All ConditionTable From SBC
    AND Get All IP2IPRouting From SBC
    AND Get All Classification From SBC
    AND Get All MessageManipulations From SBC
    AND Get All NATTranslation From SBC
    AND Get All GwRoutingPolicy From SBC
    AND Get All DialPlanRule From SBC
    Then log to console    PASS


