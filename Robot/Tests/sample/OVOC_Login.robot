*** Settings ***
Suite Setup       New Browser    ${BROWSER}    headless=${HEADLESSMODE}
Suite Teardown    Close Browser
Force Tags        SAMPLE    INPROGRESS
Resource          ./settings.robot

#Author     Shilpa Thorat

*** Variables ***
#&{OVOC_user_credentials}    username=acladmin    password=pass_1234

*** Test Cases ***
Login To OVOC As SysAdmin User And Get Version
    Given New Context And New Page For OVOC Without Creds
    When Login To OVOC With OVOC_WIN_SYSADMIN_CREDENTIALS User
    Then Get OVOC Version
