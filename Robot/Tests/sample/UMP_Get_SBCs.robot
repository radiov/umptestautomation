*** Settings ***
Suite Setup       New Browser    ${BROWSER}    headless=${HEADLESSMODE}
Suite Teardown    Close Browser
Force Tags        SAMPLE    INPROGRESS
Resource          ./settings.robot

##Author     Shilpa Thorat

*** Test Cases ***
Test To Retrieve SBCs Added In UMP
    Given New Context And New Page For UMP With UMP_ADMIN_CREDENTIALS
    AND I Login to UMP And Validate The Login
    When I Retrieve All Added SBCs In UMP With UMP_ADMIN_CREDENTIALS
    Then Validate Retrieved SBCs


