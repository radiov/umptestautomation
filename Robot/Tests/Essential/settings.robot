*** Settings ***
Documentation       Resources required for Sanity test cases.
Resource            ../../Keywords/common.robot
Resource            ../../Keywords/ump-specific.robot
Resource            ../../Keywords/ovoc-specific.robot
Resource            ../../Keywords/sbc-specific.robot
Resource            ../../Keywords/microsoft-specific.robot

